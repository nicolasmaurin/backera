package backera.models.user;


import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
public class FirstName {

    @NotNull
    @Size(min=1, max=30, message="Invalid length.")
    private String firstName;

    public FirstName(String lastName) {
        this.firstName = lastName;
    }

    public String getValue() {
        return firstName;
    }

    public void setValue(String lastName) {
        this.firstName = lastName;
    }
}
