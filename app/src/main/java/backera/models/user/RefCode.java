package backera.models.user;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
public class RefCode {

    @NotNull
    @Size(min=1, max=30, message="Invalid length.")
    private String refCode;


    public RefCode() {
    }

    public RefCode(@NotNull @Size(min = 1, max = 30, message = "Invalid length.") String reference) {
        this.refCode = reference;
    }

    public String getValue() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

}
