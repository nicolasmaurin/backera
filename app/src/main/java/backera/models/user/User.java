package backera.models.user;

import backera.DTO.UserDTO.Login;
import backera.DTO.UserDTO.Password;
import backera.DTO.UserDTO.UserDTO;
import backera.models.database.ConstraintNames;
import backera.models.message.Message;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Component
@Entity
@Table(name="user",
        uniqueConstraints = {@UniqueConstraint(name= ConstraintNames.UNIQUE_EMAIL_CONTRAINT, columnNames={"login"})}
)
@Validated
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)

    private Integer id;

    @NotNull
    @Valid
    @Email
    private String login;

    @NotNull
    @Valid
    private String password;


    @NotNull
    @Valid
    private String firstName;

    @NotNull
    @Valid
    private String lastName;

    public User() {}

    public User(UserDTO userDTO){

        this.setLogin(userDTO.getLogin());
        this.setPassword(userDTO.getPassword());
        this.setFirstName(userDTO.getFirstName());
        this.setLastName(userDTO.getLastName());
        }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }


    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}