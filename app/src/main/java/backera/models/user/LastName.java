package backera.models.user;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
public class LastName {
    @NotNull
    @Size(min=1, max=30, message="Invalid length.")
    private String lastName;

    public LastName(String lastName) {
        this.lastName = lastName;
    }

    public String getValue() {
        return lastName;
    }

    public void setValue(String lastName) {
        this.lastName = lastName;
    }
}
