package backera.controllers;

import backera.DTO.MessageDTO;
import backera.exceptions.UserNotFoundException;
import backera.models.message.Message;
import backera.repositories.MessageRepository;
import backera.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class MessagesController extends BaseController {
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private UserRepository userRepository;

    @PostMapping(path = "/messages")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    MessageDTO addMessage(@RequestBody @Validated MessageDTO rawMessage) throws Exception {
        Message message = new Message(
                rawMessage.getText(),
                userRepository.findById(rawMessage.getSenderId()).orElseThrow(UserNotFoundException::new),
                userRepository.findById(rawMessage.getRecipientId()).orElseThrow(UserNotFoundException::new)
        );
        return new MessageDTO(messageRepository.save(message));
    }

    @GetMapping(path="/messages")
    public @ResponseBody
    List<Message> getUser(@RequestParam int userId) {
        return messageRepository.findBySenderId(userId);

    }
}

