package backera.controllers;


import backera.DTO.UserDTO.UserDTO;
import backera.exceptions.ExistingUserException;
import backera.exceptions.InvalidParameterException;
import backera.exceptions.UserNotFoundException;
import backera.models.user.UsersCount;
import backera.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Controller
public class UserController extends BaseController {
    @Autowired
    private UserService userService;

    @PostMapping(path = "/users")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    UserDTO addUsers(@RequestBody @Validated UserDTO user) throws InvalidParameterException, ExistingUserException {
        user.validate(true);
        return userService.save(user);
    }

    @GetMapping(path = "/users")
    public @ResponseBody
    UserDTO getUser(UserDTO user) throws UserNotFoundException, InvalidParameterException {
        user.validate(false);
        return userService.getUser(user);
    }


    @GetMapping(path = "/users/count")
    public @ResponseBody
    UsersCount getUserCount() {
        return userService.getUserCount();

    }
}