package backera.repositories;

import backera.DTO.UserDTO.Password;
import backera.models.user.User;
import backera.DTO.UserDTO.Login;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserRepository extends CrudRepository<User, Integer> {

    Optional<User> findByLoginAndPassword(String login, String password);

    Optional<User> findByLogin(String login);
}