package backera.DTO;

public interface IValue {

    public Object getValue();
    public void setValue(Object value);

}
