package backera.DTO.UserDTO;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Embeddable;
import javax.validation.constraints.Size;


public class Password {

    private String password;

    public Password(){}

    public Password(String password){
        this.setPassword(password);
    }

    public String getValue() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void selfHash(){
        this.password = passwordEncoder().encode(this.password);
    }

    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public Boolean match(Password password){
        return passwordEncoder().matches(password.getValue(), this.password);
    }
}
