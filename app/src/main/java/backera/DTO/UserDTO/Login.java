package backera.DTO.UserDTO;

import backera.exceptions.InvalidParameterException;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Embeddable
public class Login {

    @NotNull
    @Size(min=1, max=30, message="Invalid length.")
    private String value;

    public Login(){};


    public Login(String login) throws InvalidParameterException {

        this.setValue(login);
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) throws InvalidParameterException {
        System.out.println("'''''''''''''''''''''''''''");
        System.out.println(value == null);
        if ( value == null || value.length() < 8 || value.length() > 30) {
            throw new InvalidParameterException("login is invalid");
        }
        this.value = value;
    }
}
