package backera.DTO.UserDTO;

import backera.exceptions.InvalidParameterException;
import backera.models.user.User;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Validated
public class UserDTO {
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @Valid
    @Size(min = 8, max = 60, message = "Invalid length.")
    private String login;
    private String password;
    private String firstName;
    private String lastName;

    public String getPassword() {
        return password;
    }

    public UserDTO(User user) {
        this.login = user.getLogin();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
    }

    public UserDTO() {
    }

    public UserDTO(String login, String password) throws InvalidParameterException {
        this.setLogin(login);
        this.setPassword(password);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {

        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public void validate(Boolean fullValidation) throws InvalidParameterException {

        ArrayList<String> invalidParameters = new ArrayList();
        if (!validateEmail(login)) {
            invalidParameters.add("login");
        }
        if (password == null || password.length() < 8 || password.length() > 30) {
            invalidParameters.add("password");
        }
        if (fullValidation) {
            if (firstName == null || firstName.length() < 1 || firstName.length() > 30) {
                invalidParameters.add("firstName");
            }
            if (lastName == null || lastName.length() < 1 || lastName.length() > 60) {
                invalidParameters.add("lastName");
            }
        }
        if (invalidParameters.size() >= 2) {
            throw new InvalidParameterException(
                    invalidParameters.stream()
                            .map(Object::toString)
                            .collect(Collectors.joining(", ")) + " are invalid"
            );
        } else if (invalidParameters.size() == 1) {
            throw new InvalidParameterException(
                    invalidParameters.get(0) + " is invalid"
            );
        }
    }
}
