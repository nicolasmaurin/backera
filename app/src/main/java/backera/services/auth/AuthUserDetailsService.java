package backera.services.auth;

import backera.exceptions.InvalidParameterException;
import backera.exceptions.UserNotFoundException;
import backera.DTO.UserDTO.Login;
import backera.models.user.User;
import backera.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AuthUserDetailsService implements UserDetailsService  {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            User user = userRepository.findByLogin(username).orElseThrow(UserNotFoundException::new);
            return new AuthUser(user);
        } catch (UserNotFoundException e) {
            return null;
        }
    }
}
