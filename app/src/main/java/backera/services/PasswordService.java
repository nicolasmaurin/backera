package backera.services;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class PasswordService {

    private static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    static String hash(String rawPassword){
        return passwordEncoder().encode(rawPassword);
    }

    static boolean match(String rawPassword, String hashedPassword){
        return passwordEncoder().matches(rawPassword, hashedPassword);
    }
}
