package backera.services;

import backera.DTO.UserDTO.UserDTO;
import backera.exceptions.ExistingUserException;
import backera.exceptions.UserNotFoundException;
import backera.models.user.User;
import backera.models.user.UsersCount;
import backera.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserDTO save(UserDTO userDTO) throws ExistingUserException {
        try {
            this.getUserByLogin(userDTO.getLogin());
        } catch (UserNotFoundException e) {
            userDTO.setPassword(PasswordService.hash(userDTO.getPassword()));
            return new UserDTO(userRepository.save(new User(userDTO)));
        }
        throw new ExistingUserException();
    }


    public UserDTO getUser(UserDTO userDTO) throws UserNotFoundException {
        User user = this.getUserByLogin(userDTO.getLogin());
        if (PasswordService.match(userDTO.getPassword(), user.getPassword())) {
            return new UserDTO(user);
        } else {
            throw new UserNotFoundException();
        }
    }


    public UsersCount getUserCount() {
        return new UsersCount(userRepository.count());

    }


    private User getUserByLogin(String login) throws UserNotFoundException {
        return userRepository.findByLogin(login).orElseThrow(UserNotFoundException::new);
    }
}
